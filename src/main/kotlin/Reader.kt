import model.ArchiveStore

class Reader {
    companion object {
        @kotlin.jvm.JvmStatic
        fun main(args: Array<String>) {
            when (args.count()) {
                0 -> {/* list all programs in archive */
                val archiveStore = ArchiveStore()
                    archiveStore.listProgramsArchive()
                }
                1 -> {/* */}
                2 -> {/* */}
                else -> help()
            }
        }
        @kotlin.jvm.JvmStatic
        private fun help() {
            println("Reader")
        }
    }
}