package model

import java.util.*

data class Record(
    var trackNumber: Int = 0,
    var title: String? = null,
    val artist: String? = null,
    val album: String,
    val url: String,
    val date: Date? = null,
    var audioUrl: String? = null,
    var imgUrl: String? = null
)

class RecordStore {
    fun saveMp3(){}
}