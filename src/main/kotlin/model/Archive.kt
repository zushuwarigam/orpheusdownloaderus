package model

import com.google.gson.GsonBuilder
import org.jsoup.Jsoup
import java.io.File
import java.util.*


data class Archive(
    val url: String,
    val programs: List<Program>
)

class ArchiveStore {

    companion object {
        const val root = Settings.root
        const val programsArchive = Settings.programsArchive
    }

    fun listProgramsArchive(){

        val archive: Archive = getArchive()

        with(archive) {
            archive.programs.forEachIndexed { index, program ->
                println("$index. ${program.url.substringAfterLast("/")} - ${program.title}")
            }
        }
    }

    private fun getArchive(): Archive {
        val gson = GsonBuilder().setPrettyPrinting().create()
        val file = File("archive.json")
        var archive: Archive? = null

        try {
            println("Trying to read archive from ${file.absoluteFile}")
            archive = gson.fromJson(file.readText(), Archive::class.java)
        } catch (e: Exception) {
            println("Error in reading archive from ${file.absoluteFile}")
        }

        if (archive == null) {
            println("Read archive from $programsArchive")
            archive = Archive(programsArchive, programsarchive())
            file.writeText(gson.toJson(archive))
        }
        return archive
    }

    private fun getProgramsArchive(): Archive? {
        val gson = GsonBuilder().setPrettyPrinting().create()
        val file = File("programsarchive.json")
        var archive: Archive? = null

        try {
            println("Trying to read programs archive from ${file.absoluteFile}")
            archive = gson.fromJson(file.readText(), Archive::class.java)
        } catch (e: Exception) {
            println("Error in reading programs archive from ${file.absoluteFile}")
        }
        return archive
    }

    private fun programsarchive(): List<Program> {
        val list = LinkedList<Program>()
        Jsoup.connect(programsArchive).get().select("div.afisha-list-item")
            .forEach {
                val aitTxt = it.select("div.ait-txt").first()
                val a = aitTxt.getElementsByAttribute("href").first()
                val url = "$root${a.attr("href")}"
                val title = a.text()
                val artist: String? = aitTxt.getElementsByTag("p")?.first()?.text()
                list.add(Program(url, title, artist))
            }
        return list
    }

}
