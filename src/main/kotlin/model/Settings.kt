package model

import java.text.SimpleDateFormat

object Settings {
    const val root = "http://muzcentrum.ru"
    const val programsArchive = "http://muzcentrum.ru/orpheusradio/programsarchive"
    val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy")
    val outSimpleDateFormat = SimpleDateFormat("yyyy.MM.dd")
}