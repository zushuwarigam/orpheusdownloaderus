package model

import java.util.*

data class Program(
    val url: String,
    val title: String,
    val artist: String?,
    var list: List<Record> = LinkedList()
)

class ProgramStore {
}