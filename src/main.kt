import com.mpatric.mp3agic.ID3v2
import com.mpatric.mp3agic.ID3v24Tag
import com.mpatric.mp3agic.Mp3File
import org.jsoup.Jsoup
import java.io.File
import java.lang.Exception
import java.net.URL
import java.nio.file.Files
import java.nio.file.Paths


const val baseUrl = "http://www.muzcentrum.ru"
const val archiveUrl = "http://www.muzcentrum.ru/orpheusradio/programsarchive"

const val author = "Михаил Казиник"

const val archiveUrlKazinik = "/orpheusradio/programsarchive/musicthatcameback"
const val archivePageIteratorString = "?start="
const val archivePageIteratorStep = 10

data class Record(var blogHeader: String = "",
                  var date: String = "",
                  var description: String = "",
                  var blogPostUrl: String = "",
                  var imageUrl: String = "",
                  var audioUrl: String = "", // dimension1. Params get from jQuery function
                  var audioTitle: String = "", // dimension2
                  var programName: String = "", // dimension3
                  var programTitle: String = "") // dimension4

data class Program(var name: String = "",
                   var url: String = "",
                   var imageUrl: String = "")

fun getProgramsArchive(): ArrayList<Program> {

    val programs: ArrayList<Program> = ArrayList()

    Jsoup.connect(archiveUrl).get().run {

        select("div.afisha-list-item").forEachIndexed { _, element ->

            val program = Program()

            val picDIVBlock = element.select("div.ait-pic")

            val image = picDIVBlock.select("img").first()
            val imageUrl = image.attr("abs:src")

            val blogPost = element.select("a").first()
            val blogPostUrl = blogPost.attr("href")

            program.url = baseUrl + blogPostUrl.trim()
            program.imageUrl = imageUrl.trim()

            val txtDIVBlock = element.select("div.ait-txt")
            val postName = txtDIVBlock.select("a").first()

            program.name = postName.text()

            programs.add(program)
        }

    }

    return programs
}

fun writeMP3TAGs(recordInfo: Record, recordFile: String) {

    val mp3FIle = Mp3File(recordFile)

    val id3v2Tag: ID3v2

    if (mp3FIle.hasId3v2Tag()) {
        id3v2Tag = mp3FIle.id3v2Tag
    } else {
        id3v2Tag = ID3v24Tag()
        mp3FIle.id3v2Tag = id3v2Tag
    }

    id3v2Tag.artist = author
    id3v2Tag.title = recordInfo.audioTitle
    id3v2Tag.album = recordInfo.programName
    id3v2Tag.date = recordInfo.date
    id3v2Tag.setAlbumImage(URL(recordInfo.imageUrl).openStream().readAllBytes(), "image/jpg")
    id3v2Tag.comment = recordInfo.description
    id3v2Tag.url = recordInfo.blogPostUrl

    // Lib mp3agic cant save in the same file
    mp3FIle.save(recordFile + "_TAGed")
    File(recordFile).delete()
    File(recordFile + "_TAGed").renameTo(File(recordFile))

}

fun getInitialRecordsInfo(url: String, startPage: Int = 0, countPage: Int = 1): ArrayList<Record> {

    val records: ArrayList<Record> = ArrayList()

    for (n in 0 until countPage) {

        val page = (startPage * archivePageIteratorStep)  + (n * archivePageIteratorStep)

        Jsoup.connect(url + archivePageIteratorString + page).get().run {

            select("div.afisha-list-item").forEachIndexed { _, element ->

                val rec = Record()

                val picDIVBlock = element.select("div.ait-pic")

                // From ait-pic div block get image and post linc
                val image = picDIVBlock.select("img").first()
                val imageUrl = image.attr("abs:src")

                val blogPost = element.select("a").first()
                val blogPostUrl = blogPost.attr("href")

                rec.blogPostUrl = baseUrl + blogPostUrl.trim()
                rec.imageUrl = imageUrl.trim()

                // From ait-txt div block get description and header: time plus title. And get post date.
                val txtDIVBlock = element.select("div.ait-txt")
                val postDescription = txtDIVBlock.select("p").first()

                val postHeader = txtDIVBlock.select("div.page-header")

                rec.blogHeader = postHeader.text().trim()
                rec.date = postHeader.text().trim().split(' ')[0]
                rec.description = postDescription.text().trim()

                records.add(rec)
            }
        }
    }

    return records
}

fun getRecordsInfo(records: ArrayList<Record>): ArrayList<Record> {

    for (record in records) {

        /// println(record.blogPostUrl)

        val body = Jsoup.connect(record.blogPostUrl).get().head()

        val functions = body.getElementsByAttributeValue("type", "text/javascript")

        for (func in functions) {

            val funcText = func.toString()
            val regex = Regex("'dimension[1234]': '(.*)'")  ///"'dimension[1234]' : '(.*)',"
            val matches = regex.findAll(funcText) // func.text()

            if (matches.count() != 0) {
                for (match in matches) {

                    val data = match.value.split(":")

                    val param = data[0].replace("'", "")
                    val value = data[1].replace("'", "")

                    when (param) {
                        "dimension1" -> record.audioUrl = baseUrl + value.trim()
                        "dimension2" -> record.audioTitle = value.trim()
                        "dimension3" -> record.programName = value.trim()
                        "dimension4" -> record.programTitle = value.trim()
                        else -> { // TODO: Exception? }
                        }
                    }
                }
            }
        }
    }

    return records
}

fun downloadAudioRecord(record: Record){

    val fileSeparator = File.separator

    var downloadPath = System.getProperty("user.dir") + fileSeparator

    // println("Try download ${record.audioUrl} in $downloadPath")

    if(!File(downloadPath + fileSeparator + author).exists())
        File(downloadPath + fileSeparator + author).mkdir()

    if (!File(downloadPath + fileSeparator + author + fileSeparator + record.programName).exists())
        File(downloadPath + fileSeparator + author + fileSeparator + record.programName).mkdir()

    if(File(downloadPath + fileSeparator + author).exists() and File(downloadPath + fileSeparator + author + fileSeparator + record.programName).exists())
        downloadPath = downloadPath + author + fileSeparator + record.programName + fileSeparator

    val recordFile = downloadPath + record.blogHeader.replace(":","").replace("\"", "") + ".mp3"

    println("Write file $recordFile")

    try {
        Files.copy(URL(record.audioUrl).openStream(), Paths.get(recordFile))
    } catch (e: Exception) {
        println("PIZDA!")
        println(e.message)
    }

    writeMP3TAGs(record, recordFile)

}

fun getAudioRecords(startPage: Int = 0, countPage: Int = 1) {

    println("Download $author")

    val recordsInfo = getInitialRecordsInfo(baseUrl + archiveUrlKazinik, startPage, countPage)

    getRecordsInfo(recordsInfo)

    for (record in recordsInfo) {
        downloadAudioRecord(record)
    }

    println("${recordsInfo.count()} records downloaded")
}

fun help() {
    println("Usage: [ program ] {int} {int}")
    println("        One int param set count page to scrape")
    println("        Two int param set count page and start page to scrape")
    println("""\ (  0  o)/""")

}

fun main(args: Array<String>) {

//    val programs = getProgramsArchive()

//    for(program in programs) {
//        println(program.name)
//        println(program.url)
//        println("")
//    }

    when (args.count()) {
        0 -> {getAudioRecords(startPage = 0, countPage = 1)}
        1 -> {getAudioRecords(startPage = 0, countPage = args[0].toInt())}
        2 -> {getAudioRecords(startPage = args[1].toInt(), countPage = args[0].toInt())}
        else -> {
            help()
        }
    }
}
